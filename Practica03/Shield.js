//ASTEROIDS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
  const PI2 = 2*Math.PI;

  class Shield extends GameEngine.Sprite {
    constructor(x, y, size) {
      super(x,y, size, size, "shield.svg")
      this.isAlive = true;
      this.x = x;
      this.y = y;
      this.speed = 100;
      
      let angle = (360 * Math.random()) * Math.PI/180;
      this.vx = Math.cos(angle) * this.speed;
      this.vy = Math.sin(angle) * this.speed;
      this.size = this.radius = size;
    }

    activate(x, y) {
      this.x = x;
      this.y = y;
      this.isAlive = true;
    }

    update(elapsed) {
      this.x += this.vx *elapsed;
      this.y += this.vy *elapsed;
    }

    render(ctx) {
      if(this.isAlive)
        super.render(ctx);
      //ctx.strokeStyle = "#ff0000";
      ctx.beginPath();
      //ctx.arc(this.x, this.y, this.size, 0, PI2);
      //ctx.stroke(); 
    }
  }

  GameEngine.Shield = Shield;
  return GameEngine;
})(GameEngine || {})