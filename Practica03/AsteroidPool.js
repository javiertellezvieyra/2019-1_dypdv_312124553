//ASTEROIDS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
  class AsteroidPool {
    constructor(w, h) {
      this.game_w = w;
      this.game_h = h;

      this.initialSize = 120;

      this.numAsteroids = 1000;
      this.asteroids = [];

      for (let i=0; i<this.numAsteroids; i++) {
        this.asteroids.push(new GameEngine.Asteroid(0, 0, this.initialSize));
      }
    }

    addAsteroid() {
      let x, y;

      switch (parseInt(Math.random() * 2)) {
        case 0:
          x = -this.initialSize;
          y = Math.random() * this.game_h;
          break;
        case 1:
          x = Math.random() * this.game_w;
          y = -this.initialSize;
          break;
      }

      for (let i=0; i<this.numAsteroids; i++) {
        if (!this.asteroids[i].isAlive) {
          this.asteroids[i].activate(x, y);
          this.asteroids[i].size = this.initialSize;
          return this.asteroids[i];
        }
      }
    }

    split(old_asteroid, vxBullet, vyBullet) {
      old_asteroid = this.asteroids[old_asteroid];
      let new_asteroid = this.addAsteroid();
      if (new_asteroid) {
        new_asteroid.x = old_asteroid.x;
        new_asteroid.y = old_asteroid.y;
        new_asteroid.vx = old_asteroid.vx * Math.sin(Math.abs(vxBullet));
        new_asteroid.vy = -old_asteroid.vy * -Math.sin(Math.abs(vyBullet)); 
        old_asteroid.vy = old_asteroid.vy * -Math.sin(Math.abs(vyBullet));
        new_asteroid.size = new_asteroid.radius = old_asteroid.size;
        new_asteroid.w = old_asteroid.w = old_asteroid.w/2;
        new_asteroid.h = old_asteroid.h = old_asteroid.h/2;
        new_asteroid.hp = old_asteroid.hp;
        new_asteroid.hit();
      }

      old_asteroid.hit();
    }

    

    update(elapsed) {
      for (let i=0; i<this.numAsteroids; i++) {
        if (this.asteroids[i].isAlive) {
          this.asteroids[i].update(elapsed);
        }
      }
    }

    render(ctx) {
      for (let i=0; i<this.numAsteroids; i++) {
        if (this.asteroids[i].isAlive) {
          this.asteroids[i].render(ctx);
        }
      }
    }    
  }

  GameEngine.AsteroidPool = AsteroidPool;
  return GameEngine;
})(GameEngine || {})        