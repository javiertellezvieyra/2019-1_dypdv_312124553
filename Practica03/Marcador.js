//ASTEROIDS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    let cadena = "";
  function ponerCeros(obj){
    while (obj.length<8)
    obj = '0'+ obj;
    return obj;
  }
  class Marcador {
    constructor(puntaje) {
      this.puntaje = puntaje;
    }
    processInput() {
    }
    update(elapsed) {
        cadena = this.puntaje.toString();
        cadena = ponerCeros(cadena);    
    }
    render(ctx) {
        //Dibujamos el marcador
        ctx.fillStyle = "white";
        ctx.strokeStyle = "black";
        ctx.lineWidth = 2;
        ctx.lineJoin = "round";
        ctx.font = "30px Monospace";
        ctx.beginPath();
        ctx.fillText(cadena, 480, 35);
    }
  }
GameEngine.Marcador = Marcador;
  return GameEngine;
})(GameEngine || {})