
var GameEngine = (function(GameEngine) {
  const PI_180 = Math.PI/180;
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  let shieldX;
  let shieldY; 

  let aPool;
  let bPool;
  let estado =0;
  let tangible = true;
  let visible = true;
  let escudoActivado= true;
  var myVar;
  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      shieldX=((Math.random() * cw) + 1);
      shieldY=((Math.random() * ch) + 1); 
      this.ctx = ctx;
      this.ship = new GameEngine.Ship(cw/2, ch/2, 28);
      this.shield = new GameEngine.Shield(0,0,20);
      this.asteroidPool = new GameEngine.AsteroidPool(cw, ch);
      for(let i=0; i<1 ; i++){
        this.asteroidPool.addAsteroid();
      }
      this.vida = new GameEngine.Vidas();
      this.marcador = new GameEngine.Marcador(0);


      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.ship.processInput();
      if (KEY.isPress(KEY.SPACE)) {
        estado =1;
        this.ship.visible=true;
      } 
    }

    update(elapsed) {
      if(estado == 0){
        this.marcador.update(elapsed);
      }else if(estado == 1){
        
        if(tangible){
          this.stopParpadeo();
          visible = true;
        }
        this.shield.isAlive=escudoActivado;
        this.shield.update(elapsed);
        this.ship.visible=visible; 
        this.ship.tangible=tangible;
        aPool = this.asteroidPool.asteroids;
        bPool = this.ship.weapon.bullets;

        this.ship.update(elapsed);
        this.asteroidPool.update(elapsed);

        this.marcador.update(elapsed);
        this.vida.update(elapsed);
        this.checkBorders(this.shield);
        this.checkBorders(this.ship);

        for (let i=0; i<aPool.length; i++) {
          if (aPool[i].isAlive) {
            this.checkBorders(aPool[i]);
          }
        }

        if(this.checkCircleCollision(this.ship,this.shield)){
          tangible = false;
          this.intangibilizar(1000);
          escudoActivado = false;
          this.shield.isAlive = false;
          myVar = setInterval(this.parpadeo,150);
          function funcion(){
            setTimeout (function(){
              shieldX = ((Math.random() * cw) + 1);
              shieldY = ((Math.random() * ch) + 1);
              escudoActivado = true;
            }, 30000);
            
          }
          funcion(); 
          this.shield.x = shieldX;
          this.shield.y = shieldY;
          let angle = (360 * Math.random()) * Math.PI/180;
          this.shield.vx = Math.cos(angle) * this.shield.speed;
          this.shield.vy = Math.sin(angle) * this.shield.speed;
        }

        for (let i=0; i<aPool.length; i++) {
          for (let asteroid_i=0; asteroid_i<aPool.length; asteroid_i++) {
            if ( (aPool[asteroid_i].isAlive)&& this.ship.tangible && (this.checkCircleCollision(this.ship, aPool[asteroid_i], "nave-asteroide")) ) {
              this.asteroidPool.addAsteroid();
              this.ship.tangible = false;
              tangible = false;
              this.intangibilizar(1500);
              myVar = setInterval(this.parpadeo,50);
              this.vida.total-=1;
              this.asteroidPool.split(asteroid_i,this.ship.vx+10, this.ship.vy+10);
              this.perder();
            }
          }
        
        }

        for (let bullet_i=0; bullet_i<bPool.length; bullet_i++) {
          if (bPool[bullet_i].isAlive) {
            for (let asteroid_i=0; asteroid_i<aPool.length; asteroid_i++) {
              if ( (aPool[asteroid_i].isAlive) && (this.checkCircleCollision(bPool[bullet_i], aPool[asteroid_i], "bala-asteroide")) ) {
                bPool[bullet_i].isAlive = false;
                if(aPool[asteroid_i].hp==3)
                  this.asteroidPool.addAsteroid();
                switch(aPool[asteroid_i].hp){
                  case 4:
                    this.marcador.puntaje += 20;
                    break;
                  case 3:
                    this.marcador.puntaje += 50;
                    break;
                  case 2:
                    this.marcador.puntaje +=100;
                    break;
                  case 1:
                    this.marcador.puntaje +=150;
                    break; 
                }
                this.asteroidPool.split(asteroid_i, bPool[bullet_i].vx, bPool[bullet_i].vy);
              }
            }
          }
        }
      }
    }
    parpadeo(){  
      if(visible)
        visible = false;
      else
        visible = true;   
    }


    stopParpadeo(){
      clearInterval(myVar); 
    }

    perder(){
      if(this.vida.total <=0){
        estado= 0;
        this.marcador.puntaje =0;
        this.vida.total=3;
        this.ship.x = cw/2;
        this.ship.y = ch/2;
        this.ship.visible=false;
        clearInterval(myVar);
        this.ship.vy = this.ship.vx = 0;
        for (let i=0; i<this.asteroidPool.numAsteroids; i++) {
          if (this.asteroidPool.asteroids[i].isAlive) {
            this.asteroidPool.asteroids[i].isAlive= false;
          }
        }
        this.asteroidPool.numAsteroids=0; 
        this.asteroidPool.asteroids= [];
        this.asteroidPool = new GameEngine.AsteroidPool(cw, ch);
        for(let i=0; i<1 ; i++){
          this.asteroidPool.addAsteroid();
        }      
      }
    }
    intangibilizar(tiempo){
      function funcion(){
        setTimeout (function(){
          tangible= true;
        }, tiempo);
      }
      funcion();
    }

    checkCircleCollision(obj1, obj2, tmpmsg) {
      let dist = Math.sqrt( (obj1.x - obj2.x)*(obj1.x - obj2.x) + (obj1.y - obj2.y)*(obj1.y - obj2.y) );
      if (dist < Math.max(obj1.radius, obj2.radius)) {
        console.log("colision", tmpmsg);
        return true;
      }
      return false;
    }

    checkBorders(gameObject) {
      if (gameObject.x < -gameObject.radius) {
        gameObject.x = cw + gameObject.radius;
      }
      if (gameObject.x > cw+gameObject.radius) {
        gameObject.x = -gameObject.radius;
      }
      if (gameObject.y < -gameObject.radius) {
        gameObject.y = ch + gameObject.radius;
      }
      if (gameObject.y > ch+gameObject.radius) {
        gameObject.y = -gameObject.radius;
      }
    }

    render() {
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0, 0, cw, ch);
      this.ship.render(this.ctx);
      this.shield.render(this.ctx);
      this.asteroidPool.render(this.ctx);
      this.marcador.render(this.ctx);
      this.vida.render(this.ctx);
      if(estado == 0){
        this.ctx.beginPath();
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 5;
        this.ctx.lineJoin = "round";
        this.ctx.font = "bold 20px Monospace";
        this.ctx.fillText("Presiona space", (cw/2)-100, ch/2-50);
        this.ctx.fillText("para comenzar el juego.", (cw/2)-150, ch/2 - 30);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})