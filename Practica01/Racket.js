//PONG
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    //Ancho y alto de las raquetas
    let rect_w = 15;
    let rect_h = 60;

    class Racket {
      constructor(x, y) {
        this.x = x;
        this.y = y;  
        this.speed = 400;
        
      }
  
      update(elapsed, down) {
        if(down==true){
          if(this.y>0)
            this.y-=5
        }else if(down==false){
          if(this.y<420)
            this.y+=5
        }

      }
  
      render(ctx) {
        ctx.fillStyle = "#ffffff";
        ctx.beginPath();
        ctx.fillRect(this.x, this.y, rect_w , rect_h);
      }
    }
  
    GameEngine.Racket = Racket;
    return GameEngine;
  })(GameEngine || {})