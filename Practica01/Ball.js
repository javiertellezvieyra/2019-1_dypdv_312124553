//PONG
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    let PI2 = 2*Math.PI;
    
    class Ball {
      constructor(x, y, size) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.speed = 200;

        //Auxiliares para acotar el angulo
        let max = 45;
        let min = -45;
        //Acotamos el angulo en los primeros 45 grados desde el eje x hacia el eje y en cualquier cuadrante
        let cota= Math.round(Math.random() * (max - min) + min);
        //Definimos si la bola saldrá hacia la izquierda o hacia la derecha
        let izqder=(Math.floor(Math.random()*2+1));
        if(izqder == 2){
          izqder = -1;
        }
        this.angle = (cota * Math.random()) *Math.PI/180;
        this.vx = (izqder) * Math.cos(this.angle) * this.speed;
        this.vy = Math.sin(this.angle) * this.speed;
      }
  
      update(elapsed) {
        this.x += this.vx * elapsed;
        this.y += this.vy * elapsed;
      }
  
      render(ctx) {
        ctx.fillStyle = "#ffffff";
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.size, 0, PI2);
        ctx.fill();  
      }
    }
  
    GameEngine.Ball = Ball;
    return GameEngine;
  })(GameEngine || {})