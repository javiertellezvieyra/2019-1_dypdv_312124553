//PONG
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine){
    //Estado del juego
    let state =0;
    //Ancho del marco
    let bw, xcentro;
    //Alto del marco
    let bh, ycentro;
    //Puntos del jugador 1
    let puntosp1=0;
    //Puntos del jugador 2
    let puntosp2=0;
    //Marcador que se muestra arriba
    let marcador="";
    //Auxiliar para decir que jugador ganó
    let ganador = "";
    //Auxiliares para acotar el angulo
    let max = 200;
    let min = -200;

    let Key = {
        _pressed : {},
        
        DOWND: 83,
        UPD: 87,
        UPI: 38,
        DOWNI: 40,
        SPACE: 32,
    
        isPress: function(keyCode) {
          return this._pressed[keyCode];
        },
        onKeyDown: function(keyCode) {
          this._pressed[keyCode] = true;
        },
        onKeyUp: function(keyCode) {
          delete this._pressed[keyCode];
        }
      }

    class Game {
        constructor(ctx) {
          bw = ctx.canvas.width;
          bh = ctx.canvas.height;
          xcentro = bw/2;
          ycentro = bh/2; 
          this.ctx = ctx;
          //Radio de la pelota
          let radius = 5;
          //Creamos la pelota
          this.ball = new GameEngine.Ball((xcentro),(ycentro), radius);
          //Creamos las raquetas
          this.racket1 = new GameEngine.Racket(2,(bh/2)-20);
          this.racket2 = new GameEngine.Racket((bw-17), (bh/2)-20);
        
          //Capturamos cuando se presione una tecla
          window.addEventListener("keydown", function(evt) {
            Key.onKeyDown(evt.keyCode);
          });
          window.addEventListener("keyup", function(evt) {
            Key.onKeyUp(evt.keyCode);
          });
        }
        processInput() {
            //Decimos que hacer en el caso de ser presionadas las teclas que tomamos en cuenta
            if(Key.isPress(Key.UPD)){
                this.racket1.update(1,true);
            }
            if(Key.isPress(Key.DOWND)){
                this.racket1.update(1, false);
            }
            if(Key.isPress(Key.UPI)){
                this.racket2.update(1,true);
            }
            if(Key.isPress(Key.DOWNI)){
                this.racket2.update(1,false);
            }
            if(Key.isPress(Key.SPACE)){
                puntosp1 = 0;
                puntosp2 = 0;
                state=1;
            }
        }

        update(elapsed) {
          if(state==0){

          }else if(state == 1){
            //Checamos si la bola colisiona con alguna raqueta
            //Raqueta izquierda
            if(this.racket1.x + 15 > this.ball.x &&
               this.racket1.x < this.ball.x + 5 &&
               this.racket1.y < this.ball.y + 5 &&
               this.racket1.y + 65 > this.ball.y )
               //En caso de colisionar con una raqueta aumentamos un poco la velocidad
               this.ball.vx *= -1.05;
            //Raqueta derecha
            if(this.racket2.x + 15 > this.ball.x &&
                this.racket2.x < this.ball.x + 5 &&
                this.racket2.y < this.ball.y + 5 &&
                this.racket2.y + 65 > this.ball.y )
               //En caso de colisionar con una raqueta aumentamos un poco la velocidad
                this.ball.vx *= -1.05;
            //Acotamos la velocidad para que la pelota no sea tan rapida
            if (this.ball.vx > 800)
                this.ball.vx = 800;
            //Checamos si la bola colisiona en las paredes
              //colision en pared izq
              if (this.ball.x < this.ball.size) {
                puntosp2 +=1;
                //Reiniciamos la velocidad de la pelota e invertimos su direccion
                if(this.ball.vx < 0){
                    this.ball.vx = 180;
                }else if (this.ball.vx >= 0){
                    this.ball.vx = -180;
                }
                this.ball.x = bw/2;
                this.ball.y = bh/2;
                //Cambiamos aleatoriamente el angulo de la pelota
                let cota= Math.round(Math.random() * (max - min) + min);
                this.ball.angle = (cota * Math.random()) *Math.PI/180;
                this.ball.vy = Math.sin(this.ball.angle) * cota;
            }
              //colision en pared der
              if (this.ball.x > bw-this.ball.size) {
                puntosp1 +=1;
                //Reiniciamos la velocidad de la pelota e invertimos su direccion
                if(this.ball.vx < 0){
                    this.ball.vx = 180;
                }else if (this.ball.vx >= 0){
                    this.ball.vx = -180;
                }
                this.ball.x = bw/2;
                this.ball.y = bh/2;
                //cambiar a angulo aleatorio
                //Cambiamos aleatoriamente el angulo de la pelota
                let cota= Math.round(Math.random() * (max - min) + min);
                this.ball.angle = (cota * Math.random()) *Math.PI/180;
                this.ball.vy = Math.sin(this.ball.angle)* cota;
              }
              //colision en pared arriba
              if (this.ball.y < this.ball.size) {
                this.ball.vy *= -1;
                this.ball.y = this.ball.size;
              }
              //colision en pared abajo
              if (this.ball.y > bh-this.ball.size) {
                this.ball.vy *= -1;
                this.ball.y = bh-this.ball.size;
              }
            //Actualizamos los objetos
            this.racket1.update(elapsed);
            this.racket2.update(elapsed);  
            this.ball.update(elapsed);
          } 
            //Actualizamos el marcador
            marcador = puntosp1 + " | " + puntosp2;
            //Si un jugador ya gano se pasa al estado 3
            if(puntosp1 ==5 || puntosp2 == 5){
                state = 3;
                if(puntosp1 == 5){
                    ganador = "1";
                }else if(puntosp2 == 5){
                    ganador = "2"; 
                }
                this.racket1.y=ycentro;
                this.racket2.y=ycentro;
            }   
        }
        render(){
            //Dibujamos la pelota y las raquetas
            this.ctx.clearRect(0, 0, bw, bh);
            this.ball.render(this.ctx);
            this.racket1.render(this.ctx);
            this.racket2.render(this.ctx);

            //Dibujamos el marcador
            this.ctx.fillStyle = "white";
            this.ctx.strokeStyle = "black";
            this.ctx.lineWidth = 5;
            this.ctx.lineJoin = "round";
            this.ctx.font = "bold 40px Monospace";
            this.ctx.beginPath();
            this.ctx.fillText(marcador, (bw/2)-60, 35);
            //Dibujamos el texto de "persiona espacio para comenzar el juego"
            if (state== 0 || state ==3){
                this.ctx.font = "bold 20px Monospace";
                this.ctx.beginPath();
                this.ctx.fillText("Presiona space", (bw/2)-100, 450);
                this.ctx.fillText("para comenzar el juego.", (bw/2)-150, 470);
            }
            //Dibujamos el texto del jugador ganador
            if (state ==3){
                this.ctx.beginPath();
                this.ctx.fillText("Ha ganado el jugador " + ganador, (bw/2)-145, 400);
            }
        }   
    }
    GameEngine.Game = Game;
    return GameEngine;
  } ) (GameEngine || {})