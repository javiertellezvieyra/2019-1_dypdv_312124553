var GameEngine = (function(GameEngine) {
  let gravity = 22;

  let KEY = GameEngine.KEY;

  class Player extends GameEngine.Sprite {
    constructor(x, y, w, h) {
      super(x, y, w, h, "images/player.svg", 14, 32, 32);

      this.frameCounter = 0;
      this.framesPerChange = 8;

      this.ladderCounter = 0;
      this.ladderFramesPerChange = 12;

      this.w_2 = w/2-6;
      this.h_2 = h/2;

      this.hurt= false;

      this.intangible=false;
      this.jump_heigth = 295;
      this.inFloor = false;
      this.visible =true;

      this.speed = 200;
      this.vx = 0;
      this.vy = 0;
      this.canJump = true;
      this.trasero=false;
      this.weapon = new GameEngine.Weapon(100, .1);
    }

    processInput() {
      this.vx = 0;
      this.ladderVy = 0;
      this.tryGrabLadder = false;
      if (KEY.isPress(KEY.Z_KEY)){
        this.shooting =true;
        console.log("intangible= " + this.intangible);
        console.log("state= "+ this.state);
        console.log(this.hurt);
        
        
      }
      
      if (KEY.isReleased(KEY.SPACE)) {
        this.canJump = true;
      }
      if ((KEY.isPress(KEY.SPACE)) && (this.canJump) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
        this.canJump = false;
      }

      if ((KEY.isPress(KEY.SPACE)) && (this.inLadder)) {
        this.inLadder = false;
        this.w = 32;
        this.w_2 = 8;
      }

      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
        this.direction = -1;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
        this.direction = 1;
      }

      if (KEY.isPress(KEY.UP)) {
        this.trasero =false;
        this.tryGrabLadder = true;
        this.ladderVy = -this.speed/2;
        if (this.inLadder) {
          if (((this.ladderCounter++)%this.ladderFramesPerChange) === 0) {
            this.direction *= -1;
            super.cambioX(-1*this.direction* 6);
          }
        }
      }
      if (KEY.isPress(KEY.DOWN)) {
        this.tryGrabLadder = true;
        this.ladderVy = this.speed/2;
        if (this.inLadder) {
          
          if (((this.ladderCounter++)%this.ladderFramesPerChange) === 0) {
            if(this.trasero)
          setTimeout(this.trasero=false, 100);
            this.direction *= -1;
            super.cambioX(-1*this.direction* 6);
            if(this.y<this.ladderCounter)
              this.trasero =true;
              setTimeout(this.trasero=false, 100);
          }
        }
      }
    }

    setState() {
      if (!this.inLadder) {
        if (this.vx !== 0) {
          this.state = "walking";
        }
        else if (this.inFloor & this.state!=="shooting") {
          this.state = "still";
        }
        if (!this.inFloor) {
          this.state = "jumping";
        }
      }
      else {
        this.state = "ladder";
      }
    }

    update(elapsed) {
      this.inFloor = false;
      if(this.x>=1900 && this.x<=1930&& this.y <=500 && this.y >=422){
        this.w_2 =8;
      }
      if (!this.inLadder) {
        this.vy += gravity;
        super.update(elapsed);
      }
      else {
        this.vx = 0;
        this.vy = this.ladderVy;
        super.update(elapsed);
      }
      this.weapon.auxDelayTime += elapsed;
      if (this.shooting) {
        if (this.weapon.delayActivation < this.weapon.auxDelayTime) {          
          this.weapon.shot(this.x, this.y, 12*this.direction, 0, this.vx, this.vy, this.direction);
          this.weapon.auxDelayTime = 0;
        }
      }
      this.weapon.update(elapsed);
      
    }
    
    render(ctx) {
      if (this.hurt){
        this.state = "hurt";
      }
      if (this.state === "walking") {
        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*4);
        let theFrame = parseInt(this.frameCounter/this.framesPerChange);
        if (theFrame === 3) {
          theFrame = 1;
        }
        let picture = 1+ theFrame;
        let aPintar;
        switch (picture){
          case 1:
            if(this.shooting){
              aPintar = 8;
              setTimeout(this.shooting=false, 500);
            }else
              aPintar = 1;
            break;
          case 2:
            if(this.shooting){
              aPintar = 9;
              setTimeout(this.shooting=false, 500);
            }else
              aPintar = 2;
            break;
          case 3:
            if(this.shooting){
              aPintar = 10;
              setTimeout(this.shooting=false, 500);
            }else
              aPintar = 3;
            break;
        }
        this.currentFrame = aPintar;
      }
      else if (this.state === "still") {
        if(this.shooting){
          this.currentFrame = 7;
          setTimeout(this.shooting=false, 500);
        }else
          this.currentFrame = 0;
      }
      else if (this.state === "jumping") {
        if(this.shooting){
          this.currentFrame= 11;
          setTimeout(this.shooting=false, 500);
        }else
        this.currentFrame = 4;
      }
      else if (this.state === "ladder") {
        if(this.shooting){
          this.currentFrame = 12;
          setTimeout(this.shooting=false, 500);
        }else if(this.trasero){
          this.currentFrame = 6;
        }else
          this.currentFrame = 5;
      }else if(this.state === "hurt"){
          this.currentFrame = 13;
      }
      else if (this.shooting){
        this.currentFrame = 7 ;
      }
      this.weapon.render(ctx);
      
      if(this.visible)
        super.render(ctx);
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})