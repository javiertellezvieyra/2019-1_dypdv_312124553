//MEGAMAN
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    class Life {
        constructor(x, y, w, h ) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.vidas = 28;
            this.oportunidades = 3;
        }

        update(elapsed) {
            
        }

        render(ctx) {
            ctx.fillStyle = "#000000";
            ctx.fillRect(this.x+10, this.y, this.w ,this.h);
            ctx.fillStyle = "#F3EF86"
            let y1=94;
            for(let i=0; i<this.vidas; i++){
                ctx.fillRect(this.x+11, y1, this.w-2, 2);
                y1-=3;
            }
        }
  
    }

  GameEngine.Life = Life;
  return GameEngine;
})(GameEngine || {})