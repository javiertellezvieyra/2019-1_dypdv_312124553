var GameEngine = (function(GameEngine) {
  class Puerta {
    constructor(x, y, w, h) {
      this.type = "Puerta";
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.w_2 = w/2;
      this.h_2 = h/2;
      this.isAlive = true;
    }
  }

  GameEngine.Puerta = Puerta;
  return GameEngine;
})(GameEngine || {})