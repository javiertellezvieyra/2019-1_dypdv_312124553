//MEGAMAN
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
  const PI2 = 2*Math.PI;
  let imagen;
  class Bullet{
    constructor(speed=300, activeTime=1) {
      this.isAlive = false;
      this.speed = speed;
      this.activeTime = activeTime;
      this.auxActiveTime = 1000;
      this.x = 0;
      this.y = 0;
      this.size = 2;
      this.radius = this.size;
      imagen = new Image();
      imagen.src = "images/bullet.png";
    }

    activate(ox, oy, x, y, vx, vy, direction) {
      let rotation=-.002;
      this.x = ox + x * Math.cos(rotation) - y * Math.sin(rotation);
      this.y = oy + y * Math.cos(rotation) + x * Math.sin(rotation);
      
      this.vx =direction*(this.speed * Math.cos(rotation));
      this.vy = this.speed*Math.sin(rotation);
      this.isAlive = true;
    }

    update(elapse) {
      this.x += this.vx * elapse;
      this.y += this.vy * elapse;

      this.auxActiveTime += elapse;
      if (this.auxActiveTime > this.activeTime) {
        this.isAlive = false;
        this.auxActiveTime = 0;
      }
    }

    render(ctx) {
      ctx.drawImage(imagen,this.x,this.y);
    }
  }

  GameEngine.Bullet = Bullet;
  return GameEngine;
})(GameEngine || {})