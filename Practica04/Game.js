var GameEngine = (function(GameEngine) {
  let cw;
  let ch;
  let visible=true;
  let intangible=false;
  let cont=0;
  let cont2 = 0;
  let bPool;

  let KEY = GameEngine.KEY;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;
      this.puertaBloqueada = false;

      this.state =0;
      this.image = new Image();
      this.image.src = "images/startscreen.svg"

      this.camera = new GameEngine.Camera(70, 2040, cw, ch);
      this.player = new GameEngine.Player(0, 2096, 32, 32);

      this.level = new GameEngine.Level();
      this.vida = new GameEngine.Life(12,12,10,85);
      
      this.enemies=[];
      //enemigo 1
      this.enemies.push(new GameEngine.Enemy1(288, 2000, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(315, 2012, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(309, 2061, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(546, 2000, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(598, 2064, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(630, 2025, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(1912, 80, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(1966, 108, 16, 16));
      this.enemies.push(new GameEngine.Enemy1(1936, 122, 16, 16));
      //enemigo 2
      this.enemies.push(new GameEngine.Enemy2(856,1997, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(984,2050, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(1032,1841, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(920,1774, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(952,1743, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(872,1630, 16, 16, 1));
      this.enemies.push(new GameEngine.Enemy2(920, 1565, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(984, 1502, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(920, 1421, 16, 16, 1));
      this.enemies.push(new GameEngine.Enemy2(984, 1289, 16, 16, -1));
      this.enemies.push(new GameEngine.Enemy2(952, 1245, 16, 16, -1));      
      //enemigo 4
      this.enemies.push(new GameEngine.Enemy4(1384, 1132, 16,16, 1));
      this.enemies.push(new GameEngine.Enemy4(1495, 1098, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1415, 1100, 16,16, 1));
      this.enemies.push(new GameEngine.Enemy4(1384, 1132, 16,16, 1));
      this.enemies.push(new GameEngine.Enemy4(1400, 860, 16,16, 1));
      this.enemies.push(new GameEngine.Enemy4(1528, 860, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1432, 828, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1384, 797, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1448, 668, 16,16, 1));
      this.enemies.push(new GameEngine.Enemy4(1416, 652, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1496, 590, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1368, 365, 16,16, 1));
      this.enemies.push(new GameEngine.Enemy4(1352, 412, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1448, 381, 16,16, 0));
      this.enemies.push(new GameEngine.Enemy4(1352, 317, 16,16, 1));
      //enemigo 5
      this.enemies.push(new GameEngine.Enemy5(1104, 1136, 16, 16));
      this.enemies.push(new GameEngine.Enemy5(1142, 1104, 16, 16));
      this.enemies.push(new GameEngine.Enemy5(1166, 1072, 16, 16));
      //enemigo 6
      this.enemies.push(new GameEngine.Enemy6(2068, 358, 16, 16));
      this.enemies.push(new GameEngine.Enemy6(2068, 617, 16, 16));
      //enemigo 7
      this.enemies.push(new GameEngine.Enemy7(2020, 896, 48, 64));

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.player.processInput();
      if (KEY.isPress(KEY.Z_KEY)){
        console.log("x= "+ this.player.x+ ", y= "+this.player.y);
        console.log("camera x= "+ this.camera.x+ "camera y= "+ this.camera.y);
        console.log("vidas" + this.vida.oportunidades);
        console.log(this.state);
        
      }
      if (KEY.isPress(KEY.ENTER_KEY)){
        this.state = 1;
      }
    }
    parpadeo(){
      if(visible)
        visible = false;
      else
        visible = true; 
    }
    update(elapsed) {
      bPool = this.player.weapon.bullets;
      if(this.vida.vidas<=0)
        this.morir(this.player.x, this.player.y);
      cont += elapsed;
      cont2 +=elapsed;
      if(cont >.5 && this.player.hurt )
        this.player.hurt = false;
      if(intangible){
        if(cont2>.05){
          this.parpadeo();
          cont2 = 0;
        }
      }
      if(cont >1.5 && intangible == true){
        intangible=false;
        cont =0;
        visible =true;
      }
      this.player.visible = visible;
      this.player.intangible=intangible;
      if(this.player.x >660 && this.player.x<720 && this.player.y>2200
        || this.player.x>1202 && this.player.x<1262 && this.player.y>1188){
        this.morir(this.player.x, this.player.y,0);
      }
      if(this.player.y >=860&& this.player.x >=1800){
          this.camera.y=840;
        if(this.player.x > 3222)
          this.camera.state=1;      
      }
      if(this.player.x > 2532 && this.player.x<2800   &&this.level.bol==false){
        this.level.bol =true;
      }
      if(this.player.x >3100){
        this.level.bol = true;
        this.puertaBloqueada = true;
      }
      if ((this.player.tryGrabLadder) && (!this.player.inLadder)) {
        this.checkCollisionLadders(this.player, this.level);
      }

      this.player.update(elapsed);

      if (this.player.inLadder) {
        this.checkInLadders(this.player, this.level);
      }

      this.checkCollisionPlatforms(this.player, this.level, elapsed);

      if (this.player.inFloor) {
        this.player.inLadder = false;
      }
      
     
      this.player.setState();

      this.camera.update(this.player, this.level);
       // update enemies
       for (let i=0, l=this.enemies.length; i<l; i++) {
        if (Math.abs(this.camera.x - this.enemies[i].x) < cw/2 && (this.enemies[i].tocado==false)) {
          this.enemies[i].active = true;
        }
        if ((Math.abs(this.player.x - this.enemies[i].x) > cw/2+100 && this.enemies[i].tocado)|| (Math.abs(this.player.y - this.enemies[i].y) > ch/2+100 && this.enemies[i].tocado)) {
          this.enemies[i].tocado = false;
          console.log("tocado false");
        }
        this.enemies[i].update(elapsed, this.level, cw, ch, this.camera, this.player);
      }
      
      this.checkCollisionWalls();
      // Verificamos la colisión entre el jugador y los enemigos.
      for(let i=0; i<this.enemies.length ; i++) {
        this.checkCollisionEnemy(this.player, this.enemies[i]);
      }
      //Verificamos la colision entre las balas y los enemigos.
      for(let i=0; i<this.enemies.length; i++){
        for(let j=0; j<bPool.length; j++){
          this.checkCollisionBullet(bPool[j], this.enemies[i]);
        }
      }
    }

    checkCollisionBullet(bullet, enemy){
      if(Math.abs(bullet.x - enemy.x) < enemy.w_2 && 
         Math.abs(bullet.y - enemy.y) < enemy.h_2){
            enemy.active=false; 
            enemy.tocado=true;
            enemy.x = enemy.xi;
            enemy.y = enemy.yi;
            bullet.isAlive = false;
            console.log(enemy.tocado);
           
         }
    }
    checkCollisionEnemy(player, enemy) {
      if(Math.abs(player.x - enemy.x) < player.w_2 + enemy.w_2 && 
         Math.abs(player.y - enemy.y) < player.h_2 + enemy.h_2 && 
         !this.player.intangible&&enemy.active) {
          this.vida.vidas --;
          this.player.hurt = true;
          intangible = true;
          cont =0;
          console.log("intangible= "+ this.player.intangible);
          
      }
    } 

    morir(x, y){
      for(let i=0; i<this.enemies.length ; i++) {
        this.enemies[i].tocado = true;
        this.enemies[i].active=false;
        this.enemies[i].x= this.enemies[i].xi;
        this.enemies[i].y= this.enemies[i].yi;
      }
      this.vida.oportunidades -=1;
      this.vida.vidas=28;
      if(this.vida.oportunidades<=0){
        this.reinicio();
      }
      this.player.vy =0;
      if(x<=990 && y>=1905){
        this.player.x= 14;
        this.player.y=2096;
      }else if(x<=1523 && y>=948 && y <1905){
          this.player.x=1024;
          this.player.y=1872;
      }else if(x>1300&& x<=1810 && y<890){
        this.player.x = 1450;
        this.player.y = 896;
      }else if(x>1810 && x <=2274){
        this.player.x = 1999;
        this.player.y = 176;
      }else if(x>=2360){
        this.player.x = 2360;
        this.player.y = 864;
        this.puertaBloqueada=false;
        this.camera.state=0;
      }
    }

    checkCollisionLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      let ladder;

      // top
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y-.5);
      
      
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy < 0)) {
        
        this.player.x = ladder.x;
        this.player.inLadder = true;   
        return;
      }
      

      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        console.log(ladder.type);
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      // bottom
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y+1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy > 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        this.player.trasero= true;
        return;
      }
    }

    checkInLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y + player.h_2);
      let ladder;
      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.inLadder = true;
        this.player.x = ladder.x;
        this.player.w = 16*2;
        this.player.w_2 = 8;
      }
      else {
        this.player.inLadder = false;
        this.player.w = 32;
        this.player.w_2 = 10;
      }
    }

    checkCollisionWalls() {
      //limite izquierdo
      if (this.player.x < this.camera.x -cw/2 +this.player.w_2) {
        this.player.x = this.camera.x -cw/2 +this.player.w_2;
      }
      //limite derecho
      if (this.player.x > this.camera.x +cw/2 -this.player.w_2) {
        this.player.x = this.camera.x +cw/2 -this.player.w_2;
      }
      //limite inferior
      /*if (this.player.y > ch-this.player.h_2) {
        this.player.y = 0;
        this.player.vy = 0;
      }*/
    }

    checkCollisionPlatforms(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      //center
      this.reactCollision(player,level.getPlatform(player_tile_pos.x,   player_tile_pos.y));
      // left
      this.reactCollision(player,level.getPlatform(player_tile_pos.x-1, player_tile_pos.y));
      // right
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y));

      // top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y-1));
      // bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x,   player_tile_pos.y+1));

      // left top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y-1));
      // right top
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y-1));

      // left bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x-1, player_tile_pos.y+1));
      // right bottom
      this.reactCollision(player, level.getPlatform(player_tile_pos.x+1, player_tile_pos.y+1));
    }

    reactCollision(player, platform, type) {
      if(platform && platform.type === "Picos"){
        this.vy =0;
        this.morir(this.player.x, this.player.y, 1);
        
      }
      if(platform && platform.type === "Puerta"){
        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if (overlapX < overlapY&&this.level.bol) {
          if (player.x - platform.x > 0) {
            player.x += overlapX;
          }
          else {
            player.x -= overlapX;
          }
        }else if (overlapX > overlapY&&this.level.bol) {
          if (player.y - platform.y > 0) {
            player.y += overlapY;
            if (player.vy < 0) {
              player.vy = 0;
            }
          }
          else if(this.level.bol){
            player.y -= overlapY;
            if (player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
        console.log(this.puertaBloqueada);
        
        if(!this.puertaBloqueada)
          this.level.bol=false;
        else 
          this.player.x +=2.5;

      }else if ( platform &&
           platform.type === "Platform" &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {

        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if (overlapX < overlapY) {
          if (player.x - platform.x > 0) {
            player.x += overlapX;
          }
          else {
            player.x -= overlapX;
          }
        }
        else if (overlapX > overlapY) {
          if (player.y - platform.y > 0) {
            player.y += overlapY;
            if (player.vy < 0) {
              player.vy = 0;
            }
          }
          else {
            player.y -= overlapY;
            if (player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
      }
    }

    reinicio(){
      this.state =0;
      this.vida.vidas =28;
      this.vida.oportunidades = 3;
      this.player.x = 0;
      this.player.y = 2096;
    }

    render() {
      if(this.state==1){
        this.ctx.fillStyle = "#5ad0e1";
        this.ctx.fillRect(0, 0, cw, ch);
        this.camera.applyTransform(this.ctx);
        this.level.render(this.ctx);
        this.player.render(this.ctx);
        for (let i=0, l=this.enemies.length; i<l; i++) {
          this.enemies[i].render(this.ctx);
        }
        this.camera.releaseTransform(this.ctx);
        this.vida.render(this.ctx);  
      } else{
        this.ctx.drawImage(this.image, 0, 0);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})