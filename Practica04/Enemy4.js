var GameEngine = (function(GameEngine) {
  let gravity = 16;

  class Enemy4 {
    constructor(x, y, w, h, d) {
      this.sprite = new GameEngine.Sprite(x, y, w, h, "images/enemy04.png", 3, 16, 16);
      this.tocado=false;
      this.xi= x;
      this.yi= y;
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.frameCounter = 0;
      this.framesPerChange = 8;
      this.numFrameAnimation = 3;
      this.w_2 = w/2;
      this.h_2 = h/2;
      this.vx = -25;
      this.vy = 0;
      this.active = false;
      
    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    update(elapsed, level, cw, ch, camera, player) {
      if (this.active) {
        

        //this.x = this.lerp(this.x, player.x, 0.03);
        //this.y = this.lerp(this.y, player.y, 0.03);

       // this.sprite.direction = -1;//(this.x > player.x) ? 1 : -1;

        this.sprite.x = this.x;
        this.sprite.y = this.y;
        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*(this.numFrameAnimation));
        this.sprite.currentFrame = 0+ parseInt(this.frameCounter/this.framesPerChange);

      }
    }

    render(ctx) {
      if (this.active) {
        this.sprite.renderEnemy4(ctx);
      }
    }
  }

  GameEngine.Enemy4 = Enemy4;
  return GameEngine;
})(GameEngine || {})