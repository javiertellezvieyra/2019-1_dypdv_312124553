var GameEngine = (function(GameEngine) {
  let cont=1, cont1;
  class Sprite {
    constructor(x, y, w, h, img_path, frames, frame_w, frame_h) {
      this.x = x;
      this.y = y;
      this.vx = 0;
      this.vy = 0;
      this.vr = 0;
      this.ax = 0;
      this.ay = 0;

      this.direction = 1;

      this.frames = frames;
      this.frame_w = frame_w;
      this.frame_h = frame_h;

      this.currentFrame = 0;

      this.w = w;
      this.h = h;
      this.rotation = 0;

      this.image = null;
      if (img_path) {
        this.image = new Image();
        this.image.src = img_path;
      }
    }

    processInput() { }

    cambioX(valor){
      this.w += valor;
    }

    update(elapsed) {
      this.vx += this.ax;
      this.vy += this.ay;

      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }
    
    render(ctx) {
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.rotate(this.rotation);
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*this.frame_w)%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h, 
          this.frame_w, 
          this.frame_h, 
          -this.w/2, 
          -this.h/2, 
          this.w, 
          this.h
        );
        ctx.restore();
      }
    }

    renderEnemy1(ctx) {
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.rotate(this.rotation);
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*this.frame_w)%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h-6, 
          this.frame_w, 
          this.frame_h+10, 
          -this.w/2, 
          -this.h/2, 
          this.w, 
          this.h
        );
        ctx.restore();
      }
    }
    renderEnemy2(ctx) {
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.rotate(this.rotation);
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*this.frame_w)%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h, 
          this.frame_w, 
          this.frame_h, 
          -this.w/2, 
          -this.h/2-5, 
          this.w, 
          this.h
        );
        ctx.restore();
      }
    }
    renderEnemy4(ctx) {
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.rotate(this.rotation);
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*this.frame_w)%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h, 
          this.frame_w, 
          this.frame_h, 
          -this.w/2, 
          -this.h/2-5, 
          this.w, 
          this.h
        );
        ctx.restore();
      }
    }
    renderEnemy5(ctx) {
      if(cont ==1)
        cont1 = this.frame_w-2;
      cont +=1;
      this.frame_w=cont1;
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.rotate(this.rotation);
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*this.frame_w)%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h, 
          this.frame_w, 
          this.frame_h, 
          -this.w/2, 
          -this.h/2-5, 
          this.w, 
          this.h
        );
        ctx.restore();
      }
    }
    renderEnemy6(ctx) {
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.rotate(this.rotation);
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*(this.frame_w+1))%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h-5, 
          this.frame_w, 
          this.frame_h+10, 
          -this.w/2, 
          -this.h/2, 
          this.w, 
          this.h+10
        );
        ctx.restore();
      }
    }
    renderEnemy7(ctx) {
      if (this.image) {
        ctx.save();
        if(this.inLadder && !this.trasero){
          this.x += this.direction * 5;
        }
        console.log(this.frame_w);
        
        ctx.translate(parseInt(this.x), parseInt(this.y));
        ctx.scale(this.direction, 1);
        ctx.drawImage(
          this.image, 
          (this.currentFrame*(this.frame_w+144))%this.image.width, 
          parseInt((this.currentFrame*this.frame_w)/this.image.width)*this.frame_h, 
          this.frame_w+16, 
          this.frame_h+30, 
          -this.w/2, 
          -this.h/2-20, 
          this.w-20, 
          this.h
        );
        ctx.restore();
      }
    }
  }

  GameEngine.Sprite = Sprite;
  return GameEngine;
})(GameEngine || {})