//CIRCUS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {

  let KEY = GameEngine.KEY;
  let yt;
  let xm = 0;
  let xp, yp;//Variables x y y del personaje que siempre estará en el balancin
  let auxDelayTime=0;//auxiliar que nos ayudara a que el balancin cambie su posicion cada cierto tiempo
  function swap(a, b){
      let temp= a;
      a = b;
      b = temp;
  }

  class Balancin {
   
    constructor(x, y, x1, y1) {
      this.state = 1;//Estado del balancín: 0 si la parte derecha está abajo, 1 si la parte derecha está arriba
      this.x = x;     
      this.y = y;
      this.x1 = x1;
      this.y1 = y1;
      this.vx = 0;
      this.vy = 0;
      xp = x1;
      yp = y1;
    }

    moverBalancin(){
        if (this.state == 1){
            this.state =0;
            xp=this.x + 25;
            yp = this.y;
        }
        else{
            this.state=1;
            xp = this.x1;
            yp = this.y1;
        }
        return 1;
    }

    processInput() {
        if (KEY.isPress(KEY.LEFT)) {
            xm = -5;
        }
        if (KEY.isPress(KEY.RIGHT)) {
            xm = 5;
        }
        if (KEY.isPress(KEY.Z)){
            //solo se activará la "z" si han pasado .2 segundos despues de la última vez que se usó
            if(.2 < auxDelayTime){
                this.moverBalancin();
                auxDelayTime = 0;
            }
        }
    }

    update(elapsed) {
        if(this.x>50){
            if(this.x1<590){
                this.x += xm;
                this.x1 += xm; 
            }else if(this.x1 == 590){
                this.x -= 5;
                this.x1 -= 5; 
            }
        }else if(this.x== 50){
            this.x += 5;
            this.x1 += 5; 
        }
        xm =0;
        //se aumenta el valor del auxiliar de tiempo
        auxDelayTime += elapsed;
    }
    
    render(ctx) {
        //variable aux para dibujar el triangulo
        yt = Math.max(this.y, this.y1); 
        //Dibujamos el balancin
        //Dibujamos la plataforma
        if(this.state == 1){
            ctx.moveTo(this.x, this.y);
            ctx.lineTo(this.x1, this.y1);
        }else if(this.state == 0){
            ctx.moveTo(this.x,this.y1);
            ctx.lineTo(this.x1, this.y);
        }
        ctx.lineWidth = 4;
        ctx.strokeStyle = "white";
        //Dibujamos el triangulo
        ctx.moveTo(this.x+72,yt-18);
        ctx.lineTo(this.x+62, yt);
        ctx.lineTo(this.x+87, yt);
        ctx.fill();
        //Dibujamos el personaje que siempre estará en el balancin
        if(this.state == 1){
            xp = this.x1;
            yp = this.y1;
            ctx.moveTo(xp-4, yp);
            ctx.lineTo(xp-4, yp-20);
            ctx.lineTo(xp-20, yp-10);
            ctx.lineTo(xp-4, yp-20);
            ctx.lineTo(xp, yp-30);
            ctx.lineTo(xp-10, yp-25);
            ctx.lineTo(xp, yp-30);
            ctx.lineTo(xp+6, yp-20);
            ctx.lineTo(xp, yp-30);
            ctx.lineTo(xp-5, yp-40);
            ctx.lineTo(xp+12, yp-35);
            ctx.lineTo(xp, yp-30);
        }else if(this.state == 0){
            xp = this.x + 25;
            yp = this.y1;
            ctx.moveTo(xp, yp-8);
            ctx.lineTo(xp-15,yp-20);
            ctx.lineTo(xp-20, yp-2);
            ctx.lineTo(xp-15,yp-20);
            ctx.lineTo(xp-18,yp-30);
            ctx.lineTo(xp-26, yp-20);
            ctx.lineTo(xp-18,yp-30);
            ctx.lineTo(xp-6, yp-28);
            ctx.lineTo(xp-18,yp-30);
            ctx.lineTo(xp-31,yp-35);
            ctx.lineTo(xp-11, yp-40);
            ctx.lineTo(xp-18,yp-30);
        }
        ctx.fill();
        ctx.stroke();
        ctx.restore();
    }
  }

  GameEngine.Balancin = Balancin;
  return GameEngine;
})(GameEngine || {})