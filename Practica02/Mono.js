//CIRCUS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    let KEY = GameEngine.KEY;
    let xm, ym;
    let gravedad =10;
  class Mono {
    constructor(x, y,ang) {
        this.x =x;
        this.y =y;
        this.speed = 200;
        //Auxiliares para acotar el angulo
        let max = -100;
        let min = -60;
        //Acotamos el angulo
        let cota= Math.round(Math.random() * (max - min) + min);
        this.angle =ang;
        this.vx = Math.cos(this.angle) * this.speed*2.9;
        this.vy = Math.sin(this.angle) * this.speed*2.9;
    }
    processInput() {
    }

    update(elapsed) {
        //console.log("vy= " + this.vy);
        this.vy += gravedad;
        this.x += this.vx * elapsed;
        this.y += this.vy * elapsed;
        xm = this.x;
        ym = this.y;
        //Checamos colision con paredes
        if(this.x<12){
            this.vx *= -1;
            this.x += 15
        }
        if(this.x > 628){
            this.vx *= -1;
            this.x -= 15;
        }
        if(this.y < 0){
            this.vy *= -1;
        }
        
    }

    render(ctx) {
        ctx.lineWidth = 4;
        ctx.strokeStyle = "white";
        ctx.fillStyle = "#ffffff";
        ctx.beginPath();
        //punto inicial
        ctx.moveTo(xm, ym);
        ctx.lineTo(xm+12, ym+15);
        ctx.lineTo(xm,ym);
        ctx.lineTo(xm-12, ym+15);
        ctx.lineTo(xm,ym);
        //cuello
        ctx.lineTo(xm, ym -12);
        ctx.lineTo(xm-12, ym-5);
        ctx.lineTo(xm, ym-12);
        ctx.lineTo(xm+12,ym-5);
        ctx.lineTo(xm, ym-12);
        //cabeza
        ctx.lineTo(xm-11, ym-21);
        ctx.lineTo(xm+11, ym-21);
        ctx.lineTo(xm, ym-12);
        //ctx.lineTo(xm-20, ym -30);
        ctx.stroke();
        ctx.fill();

    }
  }
GameEngine.Mono = Mono;
  return GameEngine;
})(GameEngine || {})