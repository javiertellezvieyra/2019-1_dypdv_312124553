//CIRCUS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
  let cw;
  let ch;
  let xm, ym, ang;//Coordenadas de donde saldrá el personaje 
  let medio;
  let KEY = GameEngine.KEY;
  let estado=0;
  //definira de donde saldra el mono
  function valoresInciales(){
     //Definimos de trampolin saldrá el mono
     let izqder=(Math.floor(Math.random()*2+1));
     if(izqder == 2){
       xm = 25;
       ym = 410;
       ang = -45;
     }else{
       xm=615;
       ym = 410;
       ang = -90;
     }
  }

  class Game {
    constructor(ctx) {
      valoresInciales();
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;
      //creacion del marcador
      this.marcador = new GameEngine.Marcador(0);
      //creacion del indicador de vidas
      this.vidas = new GameEngine.Vidas();
      //creacion de los trampolines
      this.trampolin = new GameEngine.Trampolin(1, 429);
      this.trampolin2 = new GameEngine.Trampolin(589, 429);
      //creacion del balancin
      this.balancin = new GameEngine.Balancin((cw/2)-70, 430, (cw/2)+70, 475);
      //Creacion del mono
      this.mono = new GameEngine.Mono(xm,ym,ang);
      //crecion de los globos
      this.globosA = [];
      this.globosB= [];
      this.globosC= [];
      let xgloboA=0, xgloboB=0, xgloboC =0;

      for (let i=0; i<20; i++){
          this.globosA.push(new GameEngine.Globo(xgloboA += 32 , 120, "#F1C40F", true, true));
          this.globosB.push(new GameEngine.Globo(xgloboB += 32, 90, "#3498DB", false, true));
          this.globosC.push(new GameEngine.Globo(xgloboC += 32 , 60, "#E74C3C", true, true));
      }
      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      if (KEY.isPress(KEY.SPACE)) {
        estado = 1;
      }
      this.balancin.processInput();
    }
    //Distancia entre dos puntos
    distancia(x1, x2, y1, y2){
      let xcua = (x1-x2)*(x1-x2);
      let ycua = (y1-y2)*(y1-y2);
      let dis = Math.sqrt(xcua + ycua);
      //console.log("distancia = " + dis );
      return dis;   
   
    }
    perder(){
      this.vidas.total -= 1;
      this.mono.x = xm;
      this.mono.y = ym;
      this.mono.ang = ang;
      this.mono.vy *= -1;
      valoresInciales();
      if(this.vidas.total <= 0){
        this.marcador.puntaje=0;
        this.vidas.total =3;
        estado = 0;
        this.reinicioGlobos(this.globosA);
        this.reinicioGlobos(this.globosB);
        this.reinicioGlobos(this.globosC);
      }  
    }
    checarColisionBalancin(){
      medio=this.balancin.x + 70;
      if(this.mono.y>415){      
        //verificamos hacia que lado esta el balancin
        if(this.balancin.state == 0){
          //checamos si cayo en algun area del balancin
          if( this.mono.x>(medio-8) && this.mono.x<((medio)+35) ){
            this.mono.vy = 550;
            this.mono.vy*=-1;
            this.mono.y=415  //checamos si el mono toca el suelo   
            this.balancin.state=1;
            this.marcador.puntaje += 1;
            if(this.mono.y>465){
              this.perder();
            }   
          } else if(this.mono.x>(medio-8) && this.mono.x<((medio)+70)){
            this.mono.vy = 650;
            this.mono.vy *=-1;
            this.mono.y=415;
            this.balancin.state=1;
            this.marcador.puntaje += 1;
          }else if((this.mono.x <medio)&&this.mono.x > 50 ){
            this.perder();
          }

        }else{
          if(  this.mono.x<(medio+8) && this.mono.x>((medio)-35) ){
            this.mono.vy = 550;
            this.mono.vy*=-1;
            this.mono.y=415;
            this.balancin.state=0;
            this.marcador.puntaje += 1;
          } else if(this.mono.x<(medio+8) && this.mono.x>((medio)-70)){
            this.mono.vy = 650;
            this.mono.vy *=-1;
            this.mono.y=415;
            this.balancin.state=0;
            this.marcador.puntaje += 1;
          }else if((this.mono.x > medio) && this.mono.x<590){
            this.perder();
          }
        }
      }
    }

    checarColisionTrampolin(){
      if(this.mono.x< 62){ 
        if(this.mono.y> 425){ 
          this.mono.y = 410;
          //this.mono.vy =550;
          this.mono.vy *= -.9;
          if (this.mono.vy <0)
            this.mono.vy +=10;
          else
            this.mono.vy -=10;
        }  
      }
      if(this.mono.x> 598){
        if(this.mono.y> 425){ 
          this.mono.y = 410;
          this.mono.vy *= -.9;
          if (this.mono.vy <0)
            this.mono.vy +=10;
          else
            this.mono.vy -=10; 
        }
      }
    }

    checarColisionGlobo(a, points){
      let distMin = this.distancia(this.mono.x, a.x, this.mono.y, a.y);
      if(a.active){  
        if(distMin < 25 ){
          console.log("colision");
          a.active = false;
          if(a.izq){
            this.mono.x += 15;
            this.mono.vx *= -1;
            this.mono.vy *=-1;
          }else{
            this.mono.x -= 15;
            this.mono.vx *= -1;
            this.mono.vy *=-1;
          }
          this.marcador.puntaje+= points;
        }
      }
    }
    //funcion que nos dira si todos los globos de un color fueron reventados
    todosGlobosDesactivados(arr){
      let bool = true;
      for(let i=0; i< arr.length; i++){
          if(arr[i].active){
            bool = false;
          }
      }
      return bool;
    }
    reinicioGlobos(arr){
      for(let i=0; i<arr.length;i++){
        arr[i].active = true;
      }
    }
    //Si se acabaron los globos de alguna fila aumentamos los respectivos puntos
        //y volvemos a activarlos
    checarGlobosDesactivados(){
      if(this.todosGlobosDesactivados(this.globosA)){
        this.marcador.puntaje += 20;
        this.reinicioGlobos(this.globosA);
      }
      if(this.todosGlobosDesactivados(this.globosB)){
        this.marcador.puntaje += 50;
        this.reinicioGlobos(this.globosB);
      }
      if(this.todosGlobosDesactivados(this.globosC)){
        this.marcador.puntaje += 100;
        this.reinicioGlobos(this.globosC);
      }
    }
    update(elapsed) {   
      if(estado==0){
        this.marcador.update(elapsed);
      }else if(estado ==1){
        //checamos si el mono toca el suelo   
        if(this.mono.y>465){
          this.perder();
        }
        //checamos si el mono toca el balancin
        this.checarColisionBalancin();
        //checamos si el mono toca un trampolin
        this.checarColisionTrampolin();
        //checamos si el mono toca algún globo
        for(let i=0; i<20; i++){
            this.checarColisionGlobo(this.globosA[i], 2);
            this.checarColisionGlobo(this.globosB[i], 5);
            this.checarColisionGlobo(this.globosC[i], 9);
            this.globosA[i].update(elapsed);
            this.globosB[i].update(elapsed);
            this.globosC[i].update(elapsed);
        }
        //Si se acabaron los globos de alguna fila aumentamos los respectivos puntos
        //y volvemos a activarlos
        this.checarGlobosDesactivados();
        
        this.balancin.update(elapsed);
        this.trampolin.update(elapsed);
        this.marcador.update(elapsed);
        this.vidas.update(elapsed);
        this.mono.update(elapsed);
      }  
    }

    render() {
      this.ctx.clearRect(0,0,cw,ch);
      if(estado == 0){
        this.ctx.beginPath();
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 5;
        this.ctx.lineJoin = "round";
        this.ctx.font = "bold 20px Monospace";
        this.ctx.fillText("Presiona space", (cw/2)-100, ch/2);
        this.ctx.fillText("para comenzar el juego.", (cw/2)-150, ch/2 + 20);
      }
        this.trampolin.render(this.ctx);
        this.trampolin2.render(this.ctx);
        this.balancin.render(this.ctx);
        this.marcador.render(this.ctx);
        this.vidas.render(this.ctx);
        this.mono.render(this.ctx);
        for(let i=0; i<20; i++){
            this.globosA[i].render(this.ctx);
            this.globosB[i].render(this.ctx);
            this.globosC[i].render(this.ctx);
        }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})