//CIRCUS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
  class Vidas {

    constructor() {
        this.total = 3;
    }
    processInput() {
    }
    update(elapsed) {
    }
    render(ctx) {
        //Dibujamos el indicador
        ctx.fillStyle = "white";
        ctx.strokeStyle = "black";
        ctx.lineWidth = 2;
        ctx.lineJoin = "round";
        ctx.font = "30px Monospace";
        ctx.beginPath();
        ctx.fillText(this.total + "♥", 580, 35);
    }
  }
GameEngine.Vidas = Vidas;
  return GameEngine;
})(GameEngine || {})