//CIRCUS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    let KEY = GameEngine.KEY;
    let cw;

  class Globo {
    constructor(x, y, color, izq, active) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.posX = x;
        this.izq = izq;
        this.size=15;
        this.active = active;
        cw =640;
    }
   
    processInput() {
    }
  
    update(elapsed) {
      //Determinamos hacia que lado se moveran
      if(this.izq){
        this.x += 2;
      }else{
        this.x -= 2;
      }
      this.checarColisionParedes();    
    }
    checarColisionParedes(){
      if (this.x < -this.size/2) {
        this.x = cw + this.size/2;
      }
      if (this.x > cw+this.size/2) {
        this.x = -this.size/2;
      }
    }
    render(ctx) {
        ctx.fillStyle = this.color;
        ctx.beginPath();
        if(this.active)
        ctx.fillRect(this.x, this.y, this.size , this.size);
    }
  }
GameEngine.Globo = Globo;
  return GameEngine;
})(GameEngine || {})