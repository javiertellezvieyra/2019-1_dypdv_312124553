//CIRCUS
//Autor Javier Téllez Vieyra
//Numero de cuenta 312124553
//Version 1.0 
var GameEngine = (function(GameEngine) {
    let KEY = GameEngine.KEY;

  class Trampolin {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    processInput() {
    }
    update(elapsed) {
    }
    render(ctx) {
        ctx.fillStyle = "#ffffff";
        ctx.beginPath();
        ctx.fillRect(this.x, this.y, 50 , 50 );
    }
  }
GameEngine.Trampolin = Trampolin;
  return GameEngine;
})(GameEngine || {})